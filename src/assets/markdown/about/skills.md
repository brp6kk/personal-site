### Skills
<!--
* ***Software:*** Microsoft Word, PowerPoint, Excel, and Access 2010, 2013, and 2016; Adobe Photoshop
* ***Programming Languages:*** C, C#, Elm, Haskell, HTML/CSS, Java, JavaScript, Python, Rust, Visual Basic
* ***JavaScript Frameworks/Libraries:*** Angular, jQuery, PixiJS, React, React Native, React Native Reanimated, Three.js
* ***Python Frameworks/Packages:*** Django, Flask, OpenCV, Pillow, PyTorch, Wand
* ***Databases:*** MongoDB, PostgreSQL
* ***Hosting Services:*** AWS Elastic Beanstalk, GitLab Pages, Heroku
* ***Version Control:*** Git, GitHub, GitLab, SVN
-->

* ***Languages:***
  * ***Proficient:*** C#, Python, JavaScript, C
  * ***Familiar:*** HTML/CSS, Java, Rust, Visual Basic, Elm
* ***Frameworks:*** .NET, Angular, Django, Flask
* ***Databases:*** SQL (Microsoft SQL Server, PostgreSQL), NoSQL (MongoDB)
* ***Tools:*** Git, GitHub, Azure DevOps, AWS, Agile Methodology
* ***Software:*** Microsoft Office Suite, Adobe Photoshop

See also relevant [college](/about/courses/undergrad) and [high school](/about/courses/high_school) courses

---
---

### Personal Projects

Click [here](/projects) to view my personal projects.