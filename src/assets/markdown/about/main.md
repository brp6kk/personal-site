Hello! I'm Eric, currently an analyst with the IT-Architecture & Engineering department at GCM Grosvenor. In the spring of 2023, I graduated from the University of Chicago with a B.S. in Computer Science (with a specialization in Machine Learning) and a minor in Linguistics.

My first encounter with computer science was a high school programming class. I quickly fell in love with this subject, especially the world of software engineering. As a full-stack developer, I enjoy using my problem-solving skills and creativity to build great products. 

While I'm originally from a small town in North Carolina, I'm currently living in Chicago, Illinois. In my spare time, I enjoy building personal projects, <a href="/about/fun">creating art, reading, and playing video games</a>.