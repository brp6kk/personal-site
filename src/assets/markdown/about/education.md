### Overview

**The University of Chicago**, Chicago, IL

*Bachelor of Science in Computer Science (Specialization in Machine Learning), Minor in Linguistics*, June 2023

**Honors Included**: Graduated Summa Cum Laude, Phi Beta Kappa, Dean's List 2022-23, Enrico Fermi Scholar in the Physical Sciences Division 3rd Year, Dean's List 2021-22, Robert Maynard Hutchins Scholar, Dean's List 2020-21, Dean's List 2019-20

[Relevant Courses](/about/courses/undergrad)

---

**Harnett Central High School**, Angier, NC

*Diploma*, June 2019

**Honors Included**: Graduated Magna Cum Laude (2019), North Carolina Scholar (2019), Outstanding Senior (2019)

[Relevant Courses](/about/courses/high_school)

---
---
### Leadership Activities

**Uncommon Hacks**, University of Chicago

*Tech Team Director of Recruitment & Member*, September 2022 - June 2023

* Collaborated with tech team to design, build, and maintain Uncommon Hacks websites
* Coordinated, led, and evaluated interviews of potential new tech team members
* Provided support at Uncommon Hacks's hackathon and makeathon events

---

**Future Business Leaders of America**, Harnett Central High School

*Secretary*, September 2018 - June 2019; *Member*, September 2017 - June 2019

* Helped organize and volunteered at fundraisers that collectively raised more than $1,000
* Recruited new members by talking to the student body and generating interest
* Competed in events at the regional, state, and national levels, receiving 1st place in the Spreadsheet Applications event at the national level (2018)

---

**National Technical Honor Society**, Harnett Central High School

*President*, September 2017 - June 2019

* Spoke at annual induction ceremonies to welcome new members and their families
* Recruited new members by talking to the student body

---
---

### Engagements

**Uncommon Hacks 2022**, Chicago, IL

*Participant*, April 2022

* Attended workshops covering topics such as machine learning
* Designed, built, and submitted a project within 24 hours

---

**Code for Good**, Remote

*Participant*, September 2021

* Applied to and was accepted to attend a hackathon in which participants collaborate with others to develop technology solutions for non-profit organizations
* Worked together with three other undergraduate students to design and build a web application meant to activate women to get involved in civic engagement
* Designed and built the frontend of the app using HTML, CSS, and Bootstrap in under 24 hours

---

**Uncommon Hacks 2021**, Remote

*Participant*, April 2021

* Attended workshops covering topics such as technical recruiting, quantum computing, frontend web development, backend development, 3D/AR, programmatic music, and game mechanics
* Designed, built, and submitted a project within 24 hours

---

**Tech Up and Down the Pacific Coast Highway Trek**, Remote

*Participant*, December 2020

* Networked with employees working in fields including research, software engineering, and data science, product management

---

**oSTEM 2020 + Out to Innovate Conference**, Remote

*Attendee*, November 2020

* Attended workshops on topics that explored being LGBT in STEM
* Networked with employers at the Career and Graduate School Expo

---

**Taking the Next Step**, Chicago, IL

*Participant*, January 2020

* Listened to panels discussing the technology industry and research sciences
* Networked with University of Chicago alumni

---
---

### Awards 

* Metcalf Fellowship Award, The College, University of Chicago (2021)
* First place, Database Design and Applications event, NC FBLA 2019 State Leadership Conference, Greensboro, NC (2019)
* Runner-up and Scholarship Recipient, FIRE Essay Contest (2018)
* First place, Database Design and Applications event, NC FBLA 2018 Southeast Regional Competition, Fayetteville, NC (2018)
* First place, Spreadsheet Applications event, FBLA-PBL 2018 National Leadership Conference, Baltimore, MD (2018)
* Third place, Spreadsheet Applications event, NC FBLA 2018 State Leadership Conference, Greensboro, NC (2018)
* First place, Spreadsheet Applications event, NC FBLA 2017 Southeast Regional Competition, Fayetteville, NC (2017)