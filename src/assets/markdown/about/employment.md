### Employment

**GCM Grosvenor**, Chicago, IL

*IT-Architecture & Engineering Analyst*, July 2023 - Present

---

**The University of Chicago**, Chicago, IL

*Grader*, March 2023 - June 2023

* Evaluated student assignments for the course CMSC 23700 Introduction to Computer Graphics
* Maintained and updated code that aids in assignment grading
* Assisted in lab sections by answering student questions

*Grader*, September 2022 - December 2022

* Evaluated student assignments for the course CMSC 28400 Introduction to Cryptography
* Provided accurate and helpful feedback to enhance student understanding

---

**GCM Grosvenor**, Chicago, IL

*IT-Architecture & Engineering Intern*, June 2022 - September 2022

---

**The University of Chicago**, Chicago, IL

*Grader*, September 2021 - December 2021

* Evaluated student assignments for the course CMSC 16100 Honors Introduction to Computer Science I
* Provided accurate and helpful feedback to enhance student understanding

---

<a href="https://www.jdwhitman.com/plasticity-project" target="_blank"><b>PLASTICITY PROJECT</b></a>, Remote

*App & Web Developer Intern*, June 2021 - August 2021

* Designed, built, and tested the RESTful backend of an app
* Incorporated persistent data storage to save images and animation data
* Used threading and multiprocessing to optimize code, cutting down backend response time
* Designed, built, and tested custom React Native component to display an animation
* Collaborated with project team to design and test the full app
* Documented code and coding process to aid future interns
* Utilized: Python programming, Flask, Pillow, MongoDB, AWS, React Native, React Native Reanimated, Git, GitHub
* See the following series of blog posts for additional details: [1](/blog/2021/9/12/summer-2021-01), [2](/blog/2021/9/12/summer-2021-02), [3](/blog/2021/9/12/summer-2021-03)

---
---

### Volunteering

PLASTICITY PROJECT, Remote

*Volunteer*, August 2021 - October 2024

* Maintained backend code and services
* Assisted in onboarding incoming interns and provided them with support as needed