## Leadership Activities

**Uncommon Hacks**, University of Chicago

*Tech Team Director of Recruitment & Member*, September 2022 - June 2023

* Collaborated with tech team to design, build, and maintain Uncommon Hacks websites
* Coordinated, led, and evaluated interviews of potential new tech team members
* Provided support at yearly Uncommon Hacks and Uncommon Makes events

---

**Future Business Leaders of America**, Harnett Central High School

*Secretary*, September 2018 - June 2019; *Member*, September 2017 - June 2019

* Helped organize and volunteered at fundraisers that collectively raised more than $1,000
* Recruited new members by talking to the student body and generating interest
* Competed in events at the regional, state, and national levels, receiving 1st place in the Spreadsheet Applications event at the national level (2018)

---

**National Technical Honor Society**, Harnett Central High School

*President*, September 2017 - June 2019

* Spoke at annual induction ceremonies to welcome new members and their families
* Recruited new members by talking to the student body

---
---

## Professional Engagements

**Uncommon Hacks 2022**, Chicago, IL

*Participant*, April 2022

* Attended workshops covering topics such as machine learning
* Designed, built, and submitted a project within 24 hours

---

**Code for Good**, Remote

*Participant*, September 2021

* Applied to and was accepted to attend a hackathon in which participants collaborate with others to develop technology solutions for non-profit organizations
* Worked together with three other undergraduate students to design and build a web application meant to activate women to get involved in civic engagement
* Designed and built the frontend of the app using HTML, CSS, and Bootstrap in under 24 hours

---

**Uncommon Hacks 2021**, Remote

*Participant*, April 2021

* Attended workshops covering topics such as technical recruiting, quantum computing, frontend web development, backend development, 3D/AR, programmatic music, and game mechanics
* Designed, built, and submitted a project within 24 hours

---

**Tech Up and Down the Pacific Coast Highway Trek**, Remote

*Participant*, December 2020

* Networked with employees working in fields including research, software engineering, and data science, product management

---

**oSTEM 2020 + Out to Innovate Conference**, Remote

*Attendee*, November 2020

* Attended workshops on topics that explored being LGBT in STEM
* Networked with employers at the Career and Graduate School Expo

---

**Taking the Next Step**, Chicago, IL

*Participant*, January 2020

* Listened to panels discussing the technology industry and research sciences
* Networked with University of Chicago alumni