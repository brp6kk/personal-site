Hoping to discuss how I can help you? Simply want to chat? Feel free to reach out!

[![Email](/assets/images/contact-icons/email.png)](mailto:ericyc@protonmail.com) ericyc@protonmail.com   
<a href="https://www.linkedin.com/in/ericyc/" target="_blank">![LinkedIn](/assets/images/contact-icons/linkedin.png)</a>  <a href="https://www.linkedin.com/in/ericyc/" target="_blank">LinkedIn</a>  
<a href="https://gitlab.com/brp6kk" target="_blank">![GitLab](/assets/images/contact-icons/gitlab.png)</a> <a href="https://gitlab.com/brp6kk" target="_blank">GitLab</a>  
<a href="https://github.com/ericyc16" target="_blank">![GitHub](/assets/images/contact-icons/github.png)</a> <a href="https://github.com/ericyc16" target="_blank">GitHub</a>  