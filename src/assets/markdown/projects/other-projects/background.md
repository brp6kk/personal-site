While I have gone into detail on some of my personal projects, others are either too small or are so old that I'm unable to go into great detail about them. However, I still want to showcase my work, and will use this page to do so.

### Advent of Code
* In December, participate in daily Christmas-themed programming puzzles
* 2024: solved 50 out of 50 puzzles
* 2023: solved 47 out of 50 puzzles
* Utilized: Python programming
* <a href="https://gitlab.com/brp6kk/advent-of-code" target="_blank">Code</a>

### Morse Learner
* Developed a CLI program to aid the user in learning morse code
* Used Pygame to dynamically generate auditory representations of morse code
* Used PyHook to monitor keypresses to determine morse code input
* Wrote test suite for unit tests
* Utilized: Python programming, Pygame, PyHook, unittest
* <a href="https://gitlab.com/brp6kk/morse-learner" target="_blank">Code</a>

### Letquiz
* Developed a flashcard-editing CLI program
* Wrote test suite for unit tests
* Incorporated persistent data storage to save study sets
* Utilized: Python programming, unittest
* <a href="https://gitlab.com/brp6kk/letquiz" target="_blank">Code</a>

### Calculator

* Developed a command-line scientific calculator
* Made use of the ReadP parser in an Applicative context to accurately interpret input
* Utilized: Haskell programming
* <a href="https://gitlab.com/brp6kk/calculator" target="_blank">Code</a>

### Tetrate

* Developed a desktop application in which a user plays a Tetris-like game
* Incorporated persistent data storage to save high scores
* Utilized: Python programming, Pygame
* <a href="https://gitlab.com/brp6kk/tetrate" target="_blank">Code</a>

### Linked

* Revised one of my old command-line applications in which two users play a connection game
* Refactored source code to apply advanced Java programming techniques
* Incorporated persistent data storage to save high scores
* Utilized: Java programming
* <a href="https://gitlab.com/brp6kk/linked" target="_blank">Code</a>
