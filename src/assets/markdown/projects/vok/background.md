When I began studying Mandarin Chinese in college, I primarily used Quizlet to study vocabulary. While I liked their game features, I found myself wanting to do database operations on my vocabulary terms as their quantity increased. Rather than searching for a replacement tool, I decided to use the opportunity to build my own.

**Vok** is a desktop application that can be seen as a combination of Microsoft Access and Quizlet. Users can create, read, update, and delete vocabulary terms. Additionally, they can set up and take tests to study these terms.

When creating or updating terms, users can specify:

* Term
* Definition
* Part of speech
* Any alternative spellings
* Characteristics
  * Essentially tags, grouping related terms. 
* Special
  * Indicates whether or not the term should have extra focus when studied

When studying terms, users can set up different types of tests:

* Flashcards
* Flashcards Draw
  * Like flashcards, with the addition of a box used to draw the answer
* Fill in the Blanks