* Programming Tool: Angular
  * TypeScript
  * Bootstrap
    * HTML & Sass
* Hosting: GitLab Pages
* Domain Name Registration: Google Domains
  * Let's Encrypt
* Version Control: Git & GitLab