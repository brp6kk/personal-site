After I moved from home into my own apartment, for the first time I had to purchase food and cook meals. Since I was living by myself, planning out meals and coming up with good (yet inexpensive) recipes was important. While writing out recipes on index cards and planning meals in a notebook worked, I thought that combining this into a web app would be useful. From this idea sprang **Massaman**, a recipe-sharing web application.

The primary feature is **recipe sharing**. All users can see a list of public recipes. Users can also sign up for an account with the site, allowing them to create their own recipes. When creating their own recipe, one can specify a title, make time, number of servings, ingredient list, instruction list, and more! After a recipe is created, it can be edited or deleted. Recipes can also be marked as "public" (able to be seen by all) or "private" (only accessible to the user).

Another feature is **meal planning**. Authenticated users can create meals, which are only visible to them. Meals can include a name, date, and time. Recipes from the site can be attached to a meal, and other non-recipe foods can also be added.

Other features that may be implemented in the future include:
* Ability to keep track of pantry ingredients
* Shopping list
* Recipe search/sort/filter functionality
* Comments for public recipes
* List of similar recipes (when viewing the details of a recipe)