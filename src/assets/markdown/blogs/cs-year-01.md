When glancing over my transcript or reviewing my CV, you'll notice that I've taken several computer science courses as I've pursued my degree in the field. What isn't so obvious, though, is what these courses actually entailed - what topics were covered in Introduction to Computer Systems? What separated Honors Introduction to Computer Science from the "regular" intro sequence? What projects did I complete in Introduction to Computer Security?

Wonder no more! In this post, I'll provide additional details on the various CS courses that I took during my first year as an undergrad at UChicago. [Part two](/blog/2021/9/4/uchicago-cs-year-two) goes into my second-year courses, and I intend on writing more posts as I finish my third and fourth years. For now, though, let's dive into my first quarter.

## Autumn 2019 

*CS courseload:*

* CMSC 16100 - Honors Introduction to Computer Science I

The intro sequence for the CS major is supposed to be just that - an introduction, assuming no prior experience. That's how CMSC 15100 Introduction to Computer Science I was marketed. 

However, this wasn't my situation. I had taken several CS courses in high school and had taught myself additional programming languages. Therefore, I was drawn to the honors option instead, described as being a good choice for "[s]tudents with programming experience and strong preparation in mathematics." 

CMSC 16100 taught introductory CS topics through Haskell, "a pure, lazy, functional programming language." Prior to  this, I had never so much as *looked* at a functional programming language. There were some mental adjustments that had to be made in order to work with this unfamiliar paradigm, but fortunately I didn't find the material too difficult to pick up. Specific topics included (but were not limited to) I/O, monads, and parsers.

While the numerous complexities of Haskell comprised a majority of the course, other material was touched upon. There was some discussion of basic data structures, such as binary trees. We also dipped our toes into propositional logic - probably the most difficult part of the class to me, what with my lack of a background in proofs (in the spring, I would take an introductory proofs course to help reduce this deficiency).

And with that, my first undergraduate CS course was complete. While quite challenging, I also enjoyed delving into unfamiliar CS territory, and I was hungry for more.

## Winter 2020 

*CS courseload:*

* CMSC 16200 - Honors Introduction to Computer Science II

Since I had succeeded in CMSC 16100, I decided to stick with the honors sequence when signing up for my winter quarter classes. CMSC 16200 used the C language as its vehicle to teach CS concepts - a language with which I was already familiar. 

Using C, we delved into numerous data structures, including binary trees, hash tables, disjoint sets, binary heaps, and graphs. Along with discussing what these structures were, we also learned ways to implement them in C, speed of operations on them (introducing me to big O notation), and how to design algorithms.  

My favorite lecture of the quarter was probably the last lecture by Fred Chong on quantum computing. I had zero prior knowledge of this field - I knew that it existed, but not much else. However, I found Professor Chong's discussion of quantum computing and his work with it to be incredibly interesting.

The end of winter quarter was when schools began converting from in-person classes to remote learning thanks to the beginning of the COVID-19 pandemic. As saddened as I was that I was going to spend my spring at home rather than in Chicago, I was looking forward to the CS course that I was going to be taking that quarter.

## Spring 2020 

*CS courseload:*

* CMSC 22000 - Introduction to Software Development

Typically, students take CMSC 15400 Introduction to Computer Systems immediately after Introduction to Computer Science II. However, as I was planning out my spring courses, I received an invitation to take CMSC 22000 Introduction to Software Development. The course sounded both fun and relevant to my career interests, so I decided to take it. While technically possible to take it and CMSC 15400 concurrently, I decided to postpone the latter class for the next year, as recommended by the professor of 22000.

My previous two CS courses were lecture based, with grades mainly determined by exams. This class, though, was different. That's not to say that there were no lectures - we learned about the software development process, diving into design, implementation, validation, deployment, and evolution of software. However, the majority of the course centered on using what was taught in lecture to develop a complex software system. 

This system was <a href="https://github.com/uchicago-cs/chiventure/" target="_blank">chiventure</a>, a text adventure game engine (or, a platform allowing game authors to develop their own text adventure games). This was a project that had started the previous year, and the students of my class worked collaboratively in teams to continue building and refining it.

I was placed on a team with four other classmates and, based on our expressed preferences, we were assigned to improve and maintain the action management features. After familiarizing ourselves with the code, we went through the issue backlog to implement new features and fix existing bugs. We also occasionally worked with other teams, such as the custom actions team, on sections of the code that spanned multiple features. Along the way, we documented issues and tasks, gave and received peer feedback, and ended with a final presentation demonstrating our accomplishments.

This class was a ton of work - despite the lack of any traditional exams, the project itself took up quite a bit of time. However, I found this work to be incredibly rewarding. As we were preparing our final presentation and reviewing what we had accomplished as a team, I couldn't help but feel proud of everything we had done. This experience strengthened my interest in software engineering, and I was excited to continue my education to further explore the world of computer science.

[Part Two: My Second Year](/blog/2021/9/4/uchicago-cs-year-two)