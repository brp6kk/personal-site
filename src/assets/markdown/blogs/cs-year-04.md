Last year, I discussed [the CS classes that I took during the 2021-2022 academic year](/blog/2022/6/5/uchicago-cs-year-three). In my fourth (and final) year as an undergraduate student, I have taken many more interesting courses. I did not take quite as many CS courses as I did the previous year; this was so that I could focus on my [linguistics minor](/blog/2023/3/20/minor-linguistics) and work for the CS department as a grader. 

## Autumn 2022

*CS courseload:*
- CMSC 25300 - Mathematical Foundations of Machine Learning

Along with taking classes, I was also working for the CS department as a grader for CMSC 28400 Introduction to Cryptography. I had taken that class in the spring of 2022 and had greatly enjoyed it. Fortunately, I was able to balance this responsibility with taking four courses: one CS course, two Linguistics, and one fun elective on Electronic Music.

### Mathematical Foundations of Machine Learning

This course is a pre-requisite for the numerous other machine learning classes offered at this school. I was initially concerned that it would be a mere re-hash of the Linear Algebra and Statistics courses I had previously taken. Fortunately, this was not the case. While there were some repeated topics (such as matrix arithmetic and least squares estimation), there were also many new topics that introduced basic machine learning concepts (such as ridge regression, kernel methods, and support vector machines). As someone with zero prior machine learning experience, I was able to learn much from this course.

We were evaluated in this course based on weekly homework sets and two exams. Each homework set involved both mathematic derivations and short coding assignments, all based on the lecture material we had covered up to that point. The coding questions were especially difficult, but it was rewarding to apply course material onto real data. The exams were two-hour in-person exams that tested us on the concepts of the course. We had one midterm exam during week 5 of the quarter, and a cumulative final during finals week. Since we were allowed one page of notes, this was not simply a test of memory. Instead, questions were similar to the mathematical derivations of the homeworks. 

As stated previously, I had no previous machine learning experience. This course helped introduce me to what can be done with this CS subfield, and it made me excited to take additional courses in this area.

## Winter 2023

*CS courseload:*
- CMSC 25040 - Introduction to Computer Vision
- CMSC 25400 - Machine Learning

### Introduction to Computer Vision

This course introduced students to the field of computer vision, a field that focuses on how computers can develop understanding from images or video. We started this course by examining some of the computer vision techniques of the past, such as using filtering for edge detection and seeking interest points for object detection. Then, we dived into more-modern approaches that utilized machine learning. We spent quite some time on convolutional neural networks and how they could be used and adjusted for computer vision tasks. We also examined things like transformers, generative adversarial networks (GANs), and self-supervised methods.

There were four homework assignments throughout the quarter, each of which involved utilizing Python programming to complete a computer vision task. The first two focused on non-ML approaches, while the last two involved building neural networks. Homework 1 required using low-level primitives to implement basic convolution, denoising, and edge detection operations. Homework 2 had us design and implement interest point detection, feature descriptor extraction, and feature matching, then using these functions for object detection. Homework 3 involved using PyTorch to build convolutional neural networks for two different tasks: image classification and image segmentation. Finally, homework 4 had us build a GAN that generated images of clothing.

There was also a final project, which allowed us to team up with a classmate to attempt another machine learning-based computer vision technique that was discussed during lecture. My partner and I chose to implement CycleGAN, a type of GAN that allows translation from one domain to another. For example, one of the examples provided by <a href="https://arxiv.org/abs/1703.10593" target="_blank">the original CycleGAN paper</a> is that the network could take a picture of a landscape during the summer and output how that same scene would look during winter. 

For this final project, though, we could not choose a translation that was already explored by that paper. Therefore, my partner and I decided to attempt to translate between selfies and pictures of anime faces. I.e., our goal was to be able to take a selfie and show how that person would look when drawn in an anime art style, and to take an anime face and output how it would look as a realistic human.

This task ended up being incredibly difficult, though we had some success. For translating from selfie to anime, the network learned that it needed to enlarge eyes and smooth textures, though it had difficulties with selfies that were not straight on. For anime to selfie, the network learned that it needed to shrink eyes and add hair and skin textures, though the results were rather uncanny. Fortunately, though, near-perfect mappings were not required for full credit for this assignment.

Overall, this course was rather difficult, but also incredibly interesting. It was insightful to learn how some of the math learned last quarter could be applied to the challenging task of computer vision.

### Machine Learning

While Introduction to Computer Vision focused on a specific application of machine learning, the Machine Learning course took a more broad approach. The first half of the course focused on an introduction of machine learning. Here, we covered such topics as regression, classification, model selection, kernels, neural networks, clustering, and dimensionality reduction. Some of these topics were briefly discussed in Mathematical Foundations of Machine Learning, but here, we went into more detail. The second half of the course delved into a statistical perspective of machine learning. We considered several probabilistic models, including logistic regression, Gaussian mixture models, and generative adversarial networks.

The assignments throughout the quarter included four projects and two exams. The projects allowed us to pair up with a classmate to solve applied problems that involved writing code to learn about data. The exams tested our knowledge of the material through mathematical reasoning and derivations. We also were provided with six problem sets throughout the quarter which, while ungraded, were meant to help prepare us for exams. The first exam focused on the first half of course material, while the second focused on the second half. We were also allowed to take in some notes to the exam period, so that the exam wasn't merely a test of memorization.

In project 1, we implemented the gradient descent algorithm to train a regression model. We additionally implemented cross-validation so as to perform model selection. For project 2, we implemented kernelized ridge regression for a variety of different kernel functions, along with the greedy forward selection algorithm as a means of feature selection. Project 3 involved writing code for forward and backward propagation of a neural network, then experimenting with network architecture to improve accuracy in a classification task. Finally, project 4 had us implement the expectation-maximization (EM) algorithm to find the parameters of a network that had the maximum likelihood (i.e., were the most likely parameters based on the given data). We also wrote an anomaly detection algorithm based on the likelihoods found for the EM algorithm.

The second half of this course was much more challenging than the first half. The first half involved some topics that were introduced in Mathematical Foundations of Machine Learning, making it so that details were easier to grasp. Additionally, the second half of this course had a probabilistic focus, and statistics is not my strong suit. However, attending office hours and intensely studying actually resulted in a final exam grade that was higher than my midterm exam grade!

## Spring 2023

*CS courseload:*
- CMSC 22200 - Computer Architecture

Along with taking classes, I again worked for the CS department as a grader, this time for the course CMSC 23700 Introduction to Computer Graphics. I had taken that class in the spring of 2022 and had greatly enjoyed it. This was fairly easy to balance along with the three courses I was taking: two Linguistics courses and one CS course.

### Computer Architecture

This course focused on two aspects of the computer system: instruction set architecture and microarchitecture. These two components interact with both the software stack and physical hardware to allow for efficient computing. Specifically, the instruction set architecture is the interface between software and hardware, which the software engineer needs to know in order to write programs. Meanwhile, the microarchitecture is the implementation of that interface. The actual topics covered in this course included basics of instruction set architecture, basics of microarchitecture, instruction level parallelism methods, caches, virtual memory, task level parallelism, and multi-core.

This course's assignments consisted of two in-class exams and four projects. The exams were open note, and required us to solve problems related to the material presented in class. Lecture slides included numerous practice problems, which I found helpful for studying. Reviewing the course textbook were also useful.

The projects required us to write a simulator (in C) of a modern microprocesser. These projects built upon each other - the first required the simulator to implement a subset of the ARMv8 instruction set. I.e., given a collection of compiled assembly code, our program had to properly execute the corresponding actions. The second project required us to implement pipelining on top of the simulator from project one, which enabled some instruction level parallelism. However, this project did not involve implementation of any branch prediction strategies - that came in the third project, which focused entirely on that. The fourth project then extended this simulator by adding level 1 instruction and data caches. For each project, we were provided with a writeup explaining the specifications of the simulator, an executable reference simulator (allowing us to determine exactly how our implemented simulator should behave), and testing scripts. While we weren't provided all of the test scripts used in grading, I found it easy to write my own tests for behavior that the provided tests did not check.

I had briefly been introduced to instruction set architecture and microarchitecture in CMSC 15400 Introduction to Computer Systems. However, this course delved much more deeply into these topics. I found the projects to be incredibly interesting and fun. While challenging, this was a great course.

## What's Next?

I no longer have to wonder what CS courses I'll be taking in upcoming years - I have now successfully finished my undergraduate education, having just graduated with a B.S. in Computer Science (with a specialization in Machine Learning). Soon, I'll start my first full-time job as an analyst with the IT-Architecture & Engineering department at GCM Grosvenor. I had interned with them last summer, and I look forward to rejoining them soon. As sad as it is to know that my time at the University of Chicago is over, I'm excited for what the future brings.