So you've built a static site using Angular, and now you want to share it with the world. Not only that, but you wish to host it under a unique, custom domain. Only question is: how do you accomplish these goals?

Not too long ago, I had these exact same questions. Since then, I have found the answers, and will be sharing them with you today. 

I used <a href="https://docs.gitlab.com/ee/user/project/pages/" target="_blank">**GitLab Pages**</a> to host my static site.

* GitLab Pages allows users to host static sites for free.
* I had already been using GitLab as my Git repository manager, so it made sense to keep my code and site hosting under one service.

I used <a href="https://domains.google/" target="_target">**Google Domains**</a> to purchase and configure my domain. 

* The registration fee covered not only the domain name, but also WHOIS privacy protection and Google name servers.
* I wanted the dev TLD, and registering a dev domain with Google Domains was cheaper than with many other services.
  * *Fun fact:* dev is owned by Google!

Other services have their own advantages and disadvantages, so be sure to do your research to find the right combination for your needs. However, I found this combo of GitLab Pages and Google Domains to work perfectly for me. With that out of the way, let's get started!

# Creating a `.gitlab-ci.yml` file

In order for GitLab to know that you wish to publish your site, you'll need to add a `.gitlab-ci.yml` file to the root of the repository. This file controls GitLab CI, or **Continuous Integration**. When using CI, all changes pushed to GitLab are built and tested automatically. The built project is then available at a URL, by default `https://[your username].gitlab.io/[repo name]`. This project can also be configured to be served at different addresses (which I will show you how to do later in this tutorial).

The contents of the file should be as follows:

```
image: node:[version]

pages:
  cache:
    paths:
    - node_modules/
  stage: deploy
  script:
  - npm install -g @angular/cli@[version]
  - npm install
  - ng build
  - cp dist/[project name]/index.html dist/[project name]/404.html
  - mkdir public
  - mv dist/[project name]/* public/
  artifacts:
    paths:
    - public
  only:
  - main
```

But what does all this mean? Let's break it down.

```
image: node:[version]
```

"Image" here refers to a Docker image. **Docker** is a platform for developing, shipping, and running applications. A **Docker image** is a template with instructions to create a container that runs on the Docker platform. Setting the Docker image informs GitLab Pages which environment is needed to build your project.

Be sure to replace [version] with the actual version of Node.js that you used.

```
pages: 
...
```

The rest of the file's content is nested under `pages:`, telling GitLab that this project is to be published under GitLab Pages.

```
  cache:
    path:
    - node_modules/
```

This adds caching for the `node_modules` directory so that it doesn't have to be built on each and every deploy.

```
  stage: deploy
```

There are three different stages for GitLab's CI/CD: build, test, and deploy. We want to deploy the website to be publicly available.

```
  script:
  - npm install -g @angular/cli@[version]
  - npm install
  - ng build
  - cp dist/[project name]/index.html dist/[project name]/404.html
  - mkdir public
  - mv dist/[project name]/* public/
```

These are the commands used as the **build instructions**. First the Angular CLI tools are installed, along with the remainder of the necessary libraries. The code is built, then moved to the `public` directory so that GitLab Pages can deploy it.

One command of note is the fourth:

```
  - cp dist/[project name]/index.html dist/[project name]/404.html
```

This copies the `index.html` file, the entry point for the site, into a new `404.html` file in the same directory. Without this command, only direct visits to the base URL (e.g., `www.[website].com`) would function as expected. Visits to other pages (e.g., `www.[website].com/page`) would display the default GitLab 404 page. This command adds a custom 404 page to be used instead, and we copy the contents of `index.html` since that file is capable of handling Angular's routing.

Be sure to change [version] and [project name] to the Angular version and project name, respectively, that you used.

```
  artifacts:
    paths:
    - public
```

Once files have been output to the `public` directory, GitLab needs to know where to get them. Specifying this under artifacts does so.

```
  only:
  - main
```

By specifying this, only deploys to the main branch affect the resulting page. If you create a new branch under a different name and push a commit to it, the CI/CD job won't run and the changes will not affect your site.

As an example to see everything filled out, below is a copy of the `.gitlab-ci.yml` for this website:

```
image: node:14.16.1

pages:
  cache:
    paths:
    - node_modules/
  stage: deploy
  script:
  - npm install -g @angular/cli@12.2.1
  - npm install
  - ng build
  - cp dist/personal-site/index.html dist/personal-site/404.html
  - mkdir public
  - mv dist/personal-site/* public/
  artifacts:
    paths:
    - public
  only:
  - main
```

*One final note:* if you deploy the site and try to visit it under the default URL used by GitLab pages (`https://[your username].gitlab.io/[repo name]`), it will not display as you might expect (in fact, visiting it for <a href="https://brp6kk.gitlab.io/personal-site/" target="_blank">this site</a> just displays a white page). This is because of the [repo name] portion of the URL. If you wish to view your site before configuring your custom domain name, you'll need to add the `base-href` option to the build command in your script:

```
  - ng build --base-href /[repo name]/
```

For me, this command looked like the following:

```
  - ng build --base-href /personal-site/
```

# Registering a domain name

Purchasing a domain name is simple enough - just go to <a href="https://domains.google" target="_blank">`domains.google`</a>, search for your desired name, and buy it. 

When checking out, I strongly recommend leaving the "Privacy protection" setting on. If you intend to keep the site up for over a year, leaving the "Auto-renew" setting on is handy.

# Adding SSL/TLS certificates from Let's Encrypt

At this point, your custom URL is only served over the default HTTP. However, it is a good idea to configure your website to be served over HTTPS. Like HTTP, HTTPS handles requests and responses when serving sites. Unlike HTTP, however, HTTPS uses SSL/TLS to encrypt these requests and responses, making visits to your website safer and more secure. Even if your website isn't necessarily handling sensitive data, it's best to enable HTTPS (I know I personally am uncomfortable with potential snooping regardless of what I'm doing on the web). 

To serve a site over HTTPS with your custom domain name, you'll need to obtain an SSL/TLS certificate from a **Certificate Authority** (CA). While some CAs charge a fee to obtain a certificate, <a href="https://letsencrypt.org/" target="_blank">**Let's Encrypt**</a> provides them for free. 

To obtain a certificate for your domain:

1. Download <a href="https://certbot.eff.org/instructions" target="_blank">**Certbot**</a> to your computer.
  * For "Software," any software will do.
  * For "System," choose the system running on your computer.
2. Open a terminal window.
  * *Windows users:* be sure to "Run as Administrator."
3. `certbot certonly --manual --preferred challenges dns`
  * *Unix users:* use `sudo` to run the command with superuser privileges.
4. When prompted to enter the domain name, enter the subdomain that you wish to use for your website.
  * You can also configure all subdomains using the wildcard character "*"
  * E.g., For this site, I entered `*.ericyc.dev`, but `www.ericyc.dev` would have also worked.
5. You will be prompted to deploy a DNS TXT record under a subdomain of the website you entered - in your browser, navigate to Google Domains, then the DNS settings for your website.
6. Click "Manage custom records" and click "Create new record."
  * Under "Host name," input the subdomain specified by Certbot.
  * Under "Type," select TXT.
  * Under "Data," input the contents specified by Certbot.
  * Save.
7. Return to your terminal and press Enter.

At this point, the terminal should display a congratulatory message that includes the save location of your certificate and chain. Make note of these locations, as you'll need the files in the next step.

~~Note that Let's Encrypt certificates expire every 90 days. To renew all certificates at once, run `certbot renew`.~~

*Edit 2021/12/8* - Certificates created manually cannot be renewed with `certbot renew`. Instead, refollow these instructions and update the certificate in GitLab pages.

# Configuring GitLab Pages and Google Domains to use your custom domain

Now that you have a valid certificate, you're almost done! 

1. Within the GitLab repository for your Angular project, navigate to the Pages tab of Settings. Click "New Domain."
2. Under "Domain," enter the subdomain you wish to use for your site.
3. Deselect "Automatic certificate management using Let's Encrypt" (after all, you should already have your own certificate).
4. Under "Certificate (PEM)," copy the contents of your certificate file.
  * The location of this file should have been provided in the congratulatory message after successfully obtaining your Let's Encrypt certificate.
5. Under "Key (PEM)," copy the contents of the key file.
  * The file location should also have been provided after obtaining the certificate.
6. In a new tab, navigate to the DNS settings for your site under Google Domains.
7. Create a new entry for your subdomain.
  * Under "Host name," enter the name for your subdomain.
  * Under "Type," select "CNAME."
  * Under "Data," enter `[your username].gitlab.io`.
8. Create a new entry to verify your subdomain.
  * Check the domain settings in GitLab Pages for the subdomain and verification message that is expected.
  * Add an entry in a similar fashion as when you were certifying the site with Let's Encrypt.
9. Return to GitLab and click the "Retry verification" button beside the "Verification status" option.

# Conclusion

At this point, you should be all set to securely access your Angular project at your custom domain!

I hope this tutorial was helpful. While specific to Angular, GitLab Pages, Let's Encrypt, and Google Domains, similar steps will likely yield similar results when using similar services. For example, from what I understand, these instructions are practically identical when deploying to GitHub Pages. I have no actual experience with that service, though, so don't hold me to that.

Any problems or questions? Feel free to [email me](mailto:ericyc@protonmail.com).

# Sources

I used the following articles as references as I configured my own site:

* <a href="https://dev.to/gaurangdhorda/deploy-angular-project-to-gitlab-pages-using-gitlab-ci-aik" target="_blank">"Deploy angular project to Gitlab pages using gitlab.ci"</a> by GaurangDhorda on DEV
* <a href="https://medium.com/@atiaxi/publishing-a-standalone-angular-app-on-gitlab-pages-b58458d2c94" target="_blank">"Publishing a Standalone Angular App on GitLab Pages"</a> by Roger Ostrander on Medium
* <a href="https://tech.jocodoma.com/2019/06/07/Setup-Custom-Domains-for-GitLab-Pages-with-SSL-TLS-Certificates/" target="_blank">"Setup Custom Domains for GitLab Pages with SSL/TLS Certificates"</a> by Joseph Chen on his tech blog
  * Ironically, as of me currently writing, this article (which included instructions on obtaining a Let's Encrypt certificate) is hosted on a blog with an expired certificate.
* <a href="https://help.datica.com/hc/en-us/articles/360044373551-Creating-and-Deploying-a-LetsEncrypt-Certificate-Manually" target="_blank">"Creating and Deploying a LetsEncrypt Certificate Manually"</a> by Tyson Nichols on Datica