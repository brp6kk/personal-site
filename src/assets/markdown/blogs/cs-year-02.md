In my [previous post](/blog/2021/9/4/uchicago-cs-year-one), I discussed the CS courses that I took my first year as an undergrad at UChicago. This post picks up right where I left off, with the courses that I took as a second-year. Without further ado, let's begin!

## Autumn 2020 

*CS courseload:*

* CMSC 15400 - Introduction to Computer Systems

In the previous quarter, I had postponed taking CMSC 15400 in favor of taking 22000 - a decision that I definitely did not regret. This quarter, I finished off the intro sequence for the CS major by taking Introduction to Computer Systems. Unlike the previous two courses, there was no honors version, but that certainly didn't mean that this class was anywhere near easy.

CMSC 15400 is notoriously one of the more difficult courses in the major. Not necessarily due to extra-challenging material - most upper-level courses are more demanding in that regard. Rather, for many (including myself), this is the first encounter with CS topics outside of programming. Additionally, the workload is quite heavy, with an abundance of challenging assignments.

This class focused on three portions of a computer system: operating systems/virtual machines, instruction set architecture, and microarchitecture. We started with an examination of bytes, learning how data is stored and operated upon. Then we dived into assembly language, followed by microarchitecture memory (focusing especially on caching). The details of operating systems were revealed, then memory management through virtual addresses and segments. Finally, we ended on synchronization.

To me, the most memorable parts of this course were the larger projects. My favorite - and also the most nerve-wracking - was the bomb project. Our task was to "defuse" a "binary bomb" - we were given an executable binary with several phases, which were "defused" by entering the proper string. Since we didn't have access to the source code, we had to reverse-engineer it by carefully stepping through the assembly code. To raise the stakes, improper inputs (which resulted in an "explosion") were automatically reported to a scoreboard, with each "explosion" lowering the final grade we received on the project. Other fun projects included writing a cache simulator and building our own shell.

Despite its reputation, CMSC 15400 was a fun course that, as with my previous CS coursework, made me even more excited to continue along my path to a computer science degree. Now that I was done with the intro sequence, there were many options of advanced CS courses from which I could take.

## Winter 2021 

*CS courseload:*

* CMSC 22300 - Functional Programming
* CMSC 23200 - Introduction to Computer Security

Many of the courses offered during the winter 2021 quarter interested me, but the two that I ended up taking were Functional Programming and Introduction to Computer Security. I chose Functional Programming because I was interested in diving into the more-advanced aspects of this paradigm after my first taste of it in CMSC 16100. Meanwhile, Introduction to Computer Security seemed like a sensible topic due to my interest in software engineering.

### Functional Programming

CMSC 22300 focused primarily on functional programming in order to:

1. Implement and analyze data structures
2. Develop interactive web applications

Our language of choice was Elm, an eager, typed functional language that compiles to JavaScript and runs in the browser. Learning Haskell in CMSC 16100 helped me easily pick up this language - in fact, I found Elm to be easier to use. Its ability to be used for web applications excited me since it showed how functional programming could be useful in the "real world."

The data structures we focused on included lists, heaps, red-black trees, and queues. For lists and queues, we implemented and analyzed both eager and lazy variations. 

Our final project was very open ended. We were required to "employ aspects of web programming and data structures in some way," but specifications were otherwise unrestricted. This project is showcased with additional details [on my site](/projects/cs223-final-project).

### Introduction to Computer Security

As with CMSC 15400, Introduction to Computer Security is well-known as one of the more difficult CS courses. Not only did we go through a very wide variety of material, there were also numerous challenging assignments. Topics included (but weren't limited to) memory vulnerabilities, web attacks and defenses, blockchain, and data privacy.

Similarly to Introduction to Computer Systems, I found the assignments to be the most memorable parts of the course. Because topics varied widely from week to week, assignments were all unique in their requirements and challenges. Highlights included:

* Crafting input that, when entered into target programs, overflowed the buffer and obtained a shell
* Developing web attacks that took advantage of CSRF, XSS, and SQL injection to exploit a (very fake) site's cryptocurrency ICO
* Setting up a server and site to track users on the web

(On the other end of the spectrum, the assignment in which we cracked passwords was more traumatizing than anything else. I'm surprised my poor laptop didn't explode due to me running Hashcat constantly for several days in a row.)

This course was definitely the most difficult that I had taken thus far. However, it was also the most rewarding - I was exposed to a broad assortment of topics, and the struggle through the assignments made completing them all the more satisfying.

## What's Next?

Spring 2021 didn't offer many CS courses in which I was interested. When pre-registration was finalized, I found that I had been placed into neither of the two courses that I had been hoping to take. However, the other non-CS courses that I had been placed into all seemed interesting, plus I had a full schedule. Therefore, rather than putting myself on the CS waitlist and hoping for an opening, I decided to take a break from CS for a quarter.

The Autumn 2021 quarter is now swiftly approaching. Unlike the previous quarter, pre-registration has placed me into CS courses - three in fact. I'm currently planning on sticking with these courses, along with a social sciences course that I'm taking as a college requirement. I'm looking forward to diving back into my CS education at UChicago, and I'm excited for my future career in this field.