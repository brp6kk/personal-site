I previously detailed [the CS classes that I took as a second-year undergrad](/blog/2021/9/4/uchicago-cs-year-two). Now, my third year is complete, and I have taken several interesting courses since then. For all quarters, I had the opportunity to take multiple computer science classes, allowing me to focus more on my major than I had in previous years.

## Autumn 2021

*CS courseload:*

* CMSC 20300 - Introduction to Human-Computer Interaction
* CMSC 27100 - Discrete Mathematics

I [previously mentioned](/blog/2021/9/4/uchicago-cs-year-two) that I was planning on taking three CS courses this quarter, but I ended up dropping one. This was because I had accepted a job offer to work as a grader for the CS department. I was responsible for grading student assignments for Honors Introduction to Computer Science I, which I had taken during [my first year](/blog/2021/9/4/uchicago-cs-year-one). I wanted to ensure that I would have sufficient time to dedicate to this responsiblility. Plus, I was especially busy during the first part of the quarter interviewing for various summer internships. Dropping a class ensured that I wouldn't be overworked. That's not to say that the two CS classes I *did* take weren't challenging, because they certainly were.

### Introduction to Human-Computer Interaction

This course introduced students to the field of HCI, focusing especially on understanding what constitutes good vs. bad design. When thinking of design for computers, I originally assumed a focus on visual-only interfaces viewed through a computer screen, or perhaps even through a smartphone. We touched on that and so much more, considering the roles of visuals, haptics, and audio as accessed through a computer screen, when immersed in VR, or inside other technology (e.g., the Amazon Echo smart speaker). The professor also presented devices built in his lab, which explores integration (not just interaction) between humans and computers. Along with all that, we touched on study planning and design prototyping.

There were a variety of projects in this course, all of which forced us to practice applying the design principles introduced in lecture. In the first, I worked with a group to design and carry out a study. Specifically, we had to design two different motion-based gestures users could perform with a smartphone, and we had to find out which of the two was more robust against accidental activation. I focused on working with another teammate to code a small webapp to detect motion data and store it in a database.

In the second, my group worked together to build an interface for scuba divers to use while underwater. We had to consider the visuals displayed on the diver's goggles, the audio coming through their headset, and the interface of a small computer available on the arm. I focused on designing and 3D printing the arm computer interface, considering such things as what buttons would be useful and how they should be arranged. I also helped do some coding in Unity of the headset interface.

The final project was an individual one where we had to design and implement a version of Pong that allowed a user to play without using their eyes or hands, therefore restricting us to an audio-only interface. Out of all of the projects, this one was my favorite. It was the one most outside of my comfort zone, and it forced me to deeply consider what makes for a good audio-only interface.

### Discrete Mathematics

The other course I took Autumn quarter was the first in the theory sequence - Discrete Mathematics. We first covered some basics by learning about logic and set theory, then got into some basic number theory - things like divisibility, modular arithmetic, and special considerations about prime numbers. Then, we discussed basic combinatorics, with topics including (but not limited to) counting and the pigeonhole principle. Finally, we ended the course on discrete probability, touching on independence, random variables, and expectation.

I was a bit intimidated going into this course - I had not had much experience with the theoretical side of computer science. However, while still tough, the class was not as painful as I feared it would be. I believe that me having previously taken the math course Introduction to Proofs in Analysis helped, along with the statistics course Statistical Methods and Applications. Discrete Mathematics refined the proof-writing skills I developed in the former as we delved into topics both familiar and new. 

Unlike the more systems-focused classes, this course did not have large projects where we wrote code to solve interesting problems. Instead, we wrote a lot of proofs. As much as I love building systems, this was still an interesting course.

## Winter 2022

*CS courseload:*

* CMSC 23500 - Introduction to Database Systems
* CMSC 27200 - Theory of Algorithms

### Introduction to Database Systems

This course introduced students to database design and implementation. We learned how to interface with a database on the user side, using tools like SQL and ORMs. However, the bulk of the class focused on the mechanisms underlying the database itself. A large focus was on how databases stored information, and how to speed up or minimize storage access through tools such as indexes and buffer pools. We also examined algorithms used to execute queries and how queries could be optimized before execution even begins. We considered transactions, their properties, and how these properties are ensured, even in events such as when the database has multiple concurrent transactions or when errors occur. Finally, we touched on distributed databases and the considerations necessary when data is stored and manipulated across multiple nodes. 

There were several assignments in this course, but the main class project was to implement parts of a relational database management system. This daunting task was split across four milestones. In the first, we began building the storage manager, which used heapfiles to store data. This milestone focused on one piece of functionality, the page. This was meant to be relatively simple as we also had to familiarize ourselves with the extensive source code.

In the second milestone, we finished building the storage manager by writing logic for heapfiles, built from the previously-implemented pages. Finally, we defined the storage manager that kept track of all of the heapfiles. This milestone took me the longest amount of time to complete - concurrency and file IO can generally be tricky, and this milestone required the use of both. 

Next we transitioned outside of the realm of storage managers and into query execution. Specifically, we had to implement the aggregate, groupby, and join operators. Other operators, such as scans and selects, were already implemented, so the focus was solely on defining these more complex operations. I began by implementing joins, writing code for both nested loop joins and hash joins. Finally, I implemented aggregation over groups. This milestone was my favorite - I imagine that was partially influenced by me not being plagued by deadlocks, like in previous milestones.

Finally, we had to improve the performance of the system. While there were some suggestions as to where to start, this milestone was less guided than the others. I decided to attempt to implement a buffer pool in my storage manager, since I had not done so previously and data access was costly without it. I had gotten through designing and writing tests when I decided to additionally explore some of the other suggestions. I found that a relatively simple change to the query optimizer improved performance by quite a lot - enough so that I could have gotten full credit just from that. Apparently, the code I had previously written was already efficient enough that this single change was all that was necessary. However, since I had already devoted so much effort to my buffer pool, I decided to go ahead and finish that up. And so I did, which further aided the system's performance.

Overall, while this course was a lot of effort, it was also one of my favorites so far in the CS major. 

### Theory of Algorithms

The second course in the CS theory sequence, Theory of Algorithms is notoriously difficult. We learned a variety of algorithm development strategies, how to prove algorithms correct, and how to prove algorithms efficient. 

We started with a gentle introduction to algorithm analysis through stable matching and the Gale-Shapley algorithm. We then moved on to consider greedy algorithms and techniques used to prove their correctness. Then we examined the divide and conquer strategy and how runtime could be determined by the Master Theorem. We next learned how we could find optimal solutions through dynamic programming, then were introduced to the maximum network flow problem and how other problems could be solved by being reduced to it. This idea of reduction followed us into the discussion of NP and computational intractability, where we had to find polynomial-time reductions to prove problems to be NP-complete.

CS majors claim this course to be super difficult for a good reason - it is, indeed, very difficult. While my proof-writing knowledge gained through previous quarters certainly helped, I found proving algorithms correct to be a different beast than proving mathematical assertions. However, through diligent practice and by asking many questions during office hours, I was able to succeed.

Like with Discrete Mathematics, this course did not feature any extensive, fun projects that I can discuss. However, like with Discrete Mathematics, I still very much enjoyed this course.

## Spring 2022

*CS courseload:*

* CMSC 23700 - Introduction to Computer Graphics
* CMSC 28400 - Introduction to Cryptography

### Introduction to Computer Graphics

This course introduced students to the broad field of computer graphics. We started by diving into the rasterization pipeline, considering how to transform objects and project them onto a screen, sample triangle coverage, and interpolate attributes. We then examined geometry, considering how to represent and process meshes. Finally, we covered animation and some of the techniques used to accomplish it.

To test our understanding of the material, the professor gave us four assignments throughout the quarter and one final project. Each assignment touched on topics covered in class - from implementing the rasterization pipeline, to manipulating triangle meshes, to rigging and skinning a 3D model. The final project was much more open - we had to implement B-splines, then choose from a list of various other graphics algorithms to implement, and finally create some sort of 10-second animation. For my extra algorithm, I chose to implement edge flips. I used that along with my B-spline implementation and my assignment 4 code (rigging and skinning a model) to create my animation:

<img src="/assets/images/blog/cs-year-03/237_final.gif" class="img-fluid" alt="Gif of cat getting bonked on the head by a glitchy rainbow dodecahedron">

The B-spline was used to approximate the dodecahedron color and camera position. Edges on the dodecahedron were randomly flipped to achieve a glitchy effect. Finally, I rigged and skinned the cat model so that its head could follow the dodecahedron (and so that it could wiggle its tail and leg).

Overall, this was a fun class. I enjoyed all of the opportunities to flex my creative muscles and appreciated learning about the numerous graphics algorithms. It's given me a greater appreciation for those who work in computer graphics!

### Introduction to Cryptography

For my final CS theory course, I had a variety to choose from. After taking Introduction to Computer Security last year, I was interested in learning more related topics. Introduction to Cryptography seemed like the perfect course for me to take.

We started by examining various classical cryptographic schemes, along with how to break them. This led us into our discussion on perfect secrecy, and how it is quite difficult to achieve. Then we dived into the main focus of the course, symmetric cryptography, considering numerous definitions of secrecy. We also touched on how authenticity could be achieved using a message authentication code (MAC). We examined properties of cryptographic hash functions before receiving a brief introduction to public key encryption.

This being a theory course, I was fully expecting to write many proofs - and I definitely did. However (perhaps due to all the practice I had the previous two quarters), I didn't find the proof-writing in this course to be especially difficult. Additionally, unlike my previous theory courses, this class did have some interesting projects.

The first project covered classical cryptography. We were given 20 ciphertexts and, without being provided any other information, were tasked with decrypting them. Through careful analysis, my group and I determined the most likely encryption schemes and set out to break them. Some were as easy as a simple shift cipher, while others (like the group encrypted via homophonic substitution) were incredibly difficult. Through a lot of patience and hard work, though, we succeeded in recovering the plaintexts.

The second project was an individual project where we were tasked with exploiting common symmetric encryption vulnerabilities to capture some flags. Half of the problems required us to utilize chosen-plaintext attacks, the other half chosen-ciphertext attacks. We had to abuse biases in pseudo-random generators, play with query lengths, and manipulate ciphertexts to recover the flag. The most difficult problem was probably the one where we had to leverage a padding oracle attack against the CBC mode of operation. Regardless of the project's difficulty, though, I really enjoyed exploring the class material in this way.

## What's Next?

During my fourth (and final) year at the University of Chicago, I just need to take two more CS electives for the major. That's not to say that I will only take two. Looking at the course catalog, there are several that I want to take so that I can broaden my CS knowledge. Hopefully by the end of the next school year, there will be plenty more CS courses for me to discuss.