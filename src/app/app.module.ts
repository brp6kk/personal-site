import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { MarkdownModule } from 'ngx-markdown';
import { HttpClientModule } from '@angular/common/http';
import { MarkdownPreviewComponent } from './components/markdown-preview/markdown-preview.component';
import { InfoCardComponent } from './components/info-card/info-card.component';
import { NavBoxComponent } from './components/nav-box/nav-box.component';
import { ImageThumbnailsComponent } from './components/image-thumbnails/image-thumbnails.component';
import { ContactComponent } from './pages/contact/contact.component';
import { ProjectsOverviewComponent } from './pages/projects-overview/projects-overview.component';
import { ProjectsDetailComponent } from './pages/projects-detail/projects-detail.component';
import { BlogOverviewComponent } from './pages/blog-overview/blog-overview.component';
import { BlogPostComponent } from './pages/blog-post/blog-post.component';
import { TestimonialsComponent } from './pages/testimonials/testimonials.component';
import { FunComponent } from './pages/fun/fun.component';
import { CoursesComponent } from './pages/courses/courses.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        AboutComponent,
        MarkdownPreviewComponent,
        InfoCardComponent,
        NavBoxComponent,
        ImageThumbnailsComponent,
        ContactComponent,
        ProjectsOverviewComponent,
        ProjectsDetailComponent,
        BlogOverviewComponent,
        BlogPostComponent,
        TestimonialsComponent,
        FunComponent,
        CoursesComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        MarkdownModule.forRoot(),
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }