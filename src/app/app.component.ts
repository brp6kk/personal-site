import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent{
    title = 'personal-site';
    activeNav? : string;

    constructor(private router: Router) {
        router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                // Scroll to top of page whenever link changes
                document.documentElement.scrollTo(0, 0);

                // Get current subsection of website
                // (home, about, projects, blog, testimonials, contact)
                let urlFull = val.url.split("/")
                let urlNav = urlFull[1]; // 0 is empty; anything after 1 is a details page

                if (urlNav.length === 0) {
                    urlNav = "home";
                }

                this.activeNav = urlNav;
            }
            
        })
    }
}
