import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlogData } from 'src/app/interfaces/blog-data';
import { SiteDataService } from 'src/app/site-data.service';

@Component({
    selector: 'app-blog-post',
    templateUrl: './blog-post.component.html',
    styleUrls: ['./blog-post.component.scss']
})
export class BlogPostComponent implements OnInit {
    postDetails? : BlogData;

    constructor(
        private router: Router,
        private route: ActivatedRoute, 
        private dataService: SiteDataService, 
    ) { }

    ngOnInit(): void {
        this.getBlogPost();
    }

    /* Get and set blog post based on the url.
     * If a blog post cannot be found, 
     * the user is rerouted to the previous page.
     */
    getBlogPost(): void {
        let year = Number(this.route.snapshot.paramMap.get('year'));
        let month = Number(this.route.snapshot.paramMap.get('month'));
        let day = Number(this.route.snapshot.paramMap.get('day'));
        let path = String(this.route.snapshot.paramMap.get('path'));

        let post = this.dataService.getBlogPost(year, month, day, path);

        if (!post) {
            this.router.navigate(['/blog']);
        } else {
            this.postDetails = post;
        }
    }
}