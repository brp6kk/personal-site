import { Component, OnInit } from '@angular/core';
import { CardData } from 'src/app/interfaces/card-data';
import { SiteDataService } from 'src/app/site-data.service';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
    mainData? : CardData;

    constructor(private dataService: SiteDataService) { }

    ngOnInit(): void {
        this.mainData = this.dataService.getContactData();
    }
}