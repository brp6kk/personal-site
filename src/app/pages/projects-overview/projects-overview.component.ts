import { Component, OnInit } from '@angular/core';
import { CardData } from 'src/app/interfaces/card-data';
import { SiteDataService } from 'src/app/site-data.service';

@Component({
    selector: 'app-projects-overview',
    templateUrl: './projects-overview.component.html',
    styleUrls: ['./projects-overview.component.scss']
})
export class ProjectsOverviewComponent implements OnInit {
    projectData? : CardData[];

    constructor(private dataService: SiteDataService) { }

    ngOnInit(): void {
        this.projectData = this.dataService.getProjectOverviews();
    }

    /* Get the local path for a project details page.
     * 
     * Parameters:
     * data - Card data
     * 
     * Returns: Local path as a string
     */
    getProjectPath = (data: CardData): string => {
        let details = this.dataService.getProjectDetailFromTitle(data.title);
        if (details) {
            return details.path;
        }

        return "";
    }
}
