import { Component, OnInit } from '@angular/core';
import { BookData } from 'src/app/interfaces/book-data';
import { ImageData } from 'src/app/interfaces/image-data';
import { NavData } from 'src/app/interfaces/nav-data';
import { SiteDataService } from 'src/app/site-data.service';

@Component({
  selector: 'app-fun',
  templateUrl: './fun.component.html',
  styleUrls: ['./fun.component.scss']
})
export class FunComponent implements OnInit {

    imageData?: ImageData[];
    bookData?: BookData[];
    selectedNav: string = "Art";

    constructor(private dataService: SiteDataService) { }

    ngOnInit(): void {      
        let funData = this.dataService.getFunData();
        
        this.imageData = funData.artwork;
        this.bookData = funData.books;
    }

}
