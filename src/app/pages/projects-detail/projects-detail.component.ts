import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SiteDataService } from 'src/app/site-data.service';
import { ProjectInfoData } from 'src/app/interfaces/project-info-data';

@Component({
    selector: 'app-projects-detail',
    templateUrl: './projects-detail.component.html',
    styleUrls: ['./projects-detail.component.scss']
})
export class ProjectsDetailComponent implements OnInit {
    projectDetails? : ProjectInfoData;

    constructor(
        private router: Router,
        private route: ActivatedRoute, 
        private dataService: SiteDataService, 
    ) { }

    ngOnInit(): void {
        this.getProjectDetails();
    }

    /* Get and set project details based on the url.
     * If a project cannot be found, 
     * the user is rerouted to the previous page.
     */
    getProjectDetails(): void {
        let path = this.route.snapshot.paramMap.get('title');
        let details = null;
        
        if (path) {
            details = this.dataService.getProjectDetailFromPath(path);
        }

        if (!details) {
            this.router.navigate(['/projects']);
        } else {
            this.projectDetails = details;
        }
    }
}