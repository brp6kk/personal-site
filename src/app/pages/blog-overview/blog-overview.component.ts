import { Component, OnInit } from '@angular/core';
import { BlogData } from 'src/app/interfaces/blog-data';
import { SiteDataService } from 'src/app/site-data.service';

@Component({
    selector: 'app-blog-overview',
    templateUrl: './blog-overview.component.html',
    styleUrls: ['./blog-overview.component.scss']
})
export class BlogOverviewComponent implements OnInit {
    blogData? : BlogData[];

    constructor(private dataService: SiteDataService) { }

    ngOnInit(): void {
        this.blogData = this.dataService.getPublicBlogData();
    }

    /* Get the local path for a blog post.
     * 
     * Parameters:
     * data - Blog post data
     * 
     * Returns: Local path as a string
     */
    getPostPath = (data: BlogData): string => {
        let date = data.date;
        let path = `/blog/${date.year}/${date.month}/${date.day}/${data.titlePath}`;
        
        return path;
    }
}