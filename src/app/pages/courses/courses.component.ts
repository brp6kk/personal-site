import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CoursesData } from 'src/app/interfaces/courses-data';
import { SiteDataService } from 'src/app/site-data.service';

@Component({
    selector: 'app-courses',
    templateUrl: './courses.component.html',
    styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
    coursesData?: CoursesData | null;

    constructor(private router: Router, private route: ActivatedRoute, private dataService: SiteDataService) { }

    ngOnInit(): void {
        this.coursesData = this.dataService.getCoursesData(this.route.snapshot.paramMap.get('school'));

        if (!this.coursesData) {
            this.router.navigate(['/about']);
        }
    }
}