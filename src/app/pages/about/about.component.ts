import { Component, OnInit } from '@angular/core';
import { CardData } from 'src/app/interfaces/card-data';
import { NavData } from 'src/app/interfaces/nav-data';
import { SiteDataService } from 'src/app/site-data.service';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
    mainData?: CardData;
    cvData?: NavData[];

    constructor(private dataService: SiteDataService) { }

    ngOnInit(): void {        
        let aboutData = this.dataService.getAboutData();
        // Data for topmost card.
        this.mainData = aboutData.main;
        // Data for bottom navigation box.
        this.cvData = aboutData.cv;
    }
}