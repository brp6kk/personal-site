import { Component, OnInit } from '@angular/core';
import { TestimonialData } from 'src/app/interfaces/testimonial-data';
import { SiteDataService } from 'src/app/site-data.service';

@Component({
    selector: 'app-testimonials',
    templateUrl: './testimonials.component.html',
    styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {
    mainData? : TestimonialData[];
    constructor(private dataService: SiteDataService) { }

    ngOnInit(): void {
        this.mainData = this.dataService.getTestimonials();
    }
}