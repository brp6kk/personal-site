import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { BlogOverviewComponent } from './pages/blog-overview/blog-overview.component';
import { BlogPostComponent } from './pages/blog-post/blog-post.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CoursesComponent } from './pages/courses/courses.component';
import { FunComponent } from './pages/fun/fun.component';
import { HomeComponent } from './pages/home/home.component';
import { ProjectsDetailComponent } from './pages/projects-detail/projects-detail.component';
import { ProjectsOverviewComponent } from './pages/projects-overview/projects-overview.component';
import { TestimonialsComponent } from './pages/testimonials/testimonials.component';

const routes: Routes = [
    { path: 'about', component: AboutComponent },
    { path: 'about/courses/:school', component: CoursesComponent },
    { path: 'about/fun', component: FunComponent },
    { path: 'projects/:title', component: ProjectsDetailComponent },
    { path: 'projects', component: ProjectsOverviewComponent },
    { path: 'blog/:year/:month/:day/:path', component: BlogPostComponent },
    { path: 'blog', component: BlogOverviewComponent },
    { path: 'testimonials', component: TestimonialsComponent },
    { path: 'contact', component: ContactComponent },
    { path: '', component: HomeComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }