import { Component, OnInit, Input } from '@angular/core';
import { MarkdownService } from 'ngx-markdown';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-markdown-preview',
    templateUrl: './markdown-preview.component.html',
    styleUrls: ['./markdown-preview.component.scss']
})
export class MarkdownPreviewComponent implements OnInit {
    @Input() filename = '';
    markdown: string | undefined;

    constructor(private mdService: MarkdownService, private http: HttpClient) { }

    async ngOnInit() {
        // Get markdown file data & compile it to be displayable
        const markdownRaw = await this.http.get(this.filename, { responseType: 'text' }).toPromise();
        this.markdown = this.mdService.compile(markdownRaw)
    }
}