import { Component, OnInit, Input } from '@angular/core';
import { CardData } from 'src/app/interfaces/card-data';

@Component({
    selector: 'app-info-card',
    templateUrl: './info-card.component.html',
    styleUrls: ['./info-card.component.scss']
})
export class InfoCardComponent implements OnInit {
    @Input() mainData?: CardData;

    constructor() { }

    ngOnInit(): void {
    }
}