import { Component, OnInit, Input } from '@angular/core';
import { NavData } from 'src/app/interfaces/nav-data';

@Component({
    selector: 'app-nav-box',
    templateUrl: './nav-box.component.html',
    styleUrls: ['./nav-box.component.scss']
})
export class NavBoxComponent implements OnInit {
    @Input() navData? : NavData[];
    selectedNav?: NavData;

    constructor() { }

    ngOnInit(): void {
        if (this.navData && this.navData.length > 0) {
            this.selectedNav = this.navData[0];
        }
    }
}