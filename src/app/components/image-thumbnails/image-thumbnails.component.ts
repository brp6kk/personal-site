import { Component, OnInit, Input } from '@angular/core';
import { ImageData } from 'src/app/interfaces/image-data';

@Component({
  selector: 'app-image-thumbnails',
  templateUrl: './image-thumbnails.component.html',
  styleUrls: ['./image-thumbnails.component.scss']
})
export class ImageThumbnailsComponent implements OnInit {
    @Input() images?: ImageData[];
    constructor() { }

    ngOnInit(): void {
    }

}
