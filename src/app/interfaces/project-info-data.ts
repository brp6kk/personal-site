import { ProjectLinkData } from "./project-link-data";

export interface ProjectInfoData {
    title: string,
    backgroundFile: string,
    technologyFile: string,
    links: ProjectLinkData[],
    images: string[],
    path: string
}