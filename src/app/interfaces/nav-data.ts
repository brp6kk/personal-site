export interface NavData {
    title: string;
    markdownFile: string;
}