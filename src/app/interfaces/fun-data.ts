import { BookData } from "./book-data";
import { ImageData } from "./image-data";

export interface FunData {
    books: BookData[];
    artwork: ImageData[];
}