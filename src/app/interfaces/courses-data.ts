export interface CoursesData {
    label: string,
    courses: { 
        code: string | null,
        title: string
    } []
}