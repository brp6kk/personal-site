export interface BookData {
    title: string;
    author: string;
}