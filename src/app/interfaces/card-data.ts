export interface CardData {
    title: string;
    image: string;
    contents: string;
    image_link?: string;
}