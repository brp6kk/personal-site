export interface BlogData {
    title: string;
    date: {
        year: number,
        month: number,
        day: number
    };
    markdownFile: string;
    titlePath: string;
    public: boolean;
}