export interface ImageData {
    id: string,
    imgSrc: string;
    thumbnailSrc: string;
    date: {
        year: number,
        month: number,
        day: number
    };
    description: string;
    medium: string;
}