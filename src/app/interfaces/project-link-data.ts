export interface ProjectLinkData {
    name: string,
    link: string
}