import { CardData } from "./card-data";
import { NavData } from "./nav-data";

export interface AboutData {
    main: CardData,
    cv: NavData[]
}