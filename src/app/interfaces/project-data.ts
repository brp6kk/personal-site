import { ProjectLinkData } from "./project-link-data";

export interface ProjectData {
    title: string,
    overview: {
        image: string,
        contents: string
    },
    details: {
        backgroundFile: string,
        technologyFile: string,
        links: ProjectLinkData[],
        images: string[],
        path: string
    }
}