import { Injectable } from '@angular/core';
import { AboutData } from './interfaces/about-data';
import * as data from '../assets/site-data.json';
import { CardData } from './interfaces/card-data';
import { ProjectData } from './interfaces/project-data';
import { ProjectInfoData } from './interfaces/project-info-data';
import { BlogData } from './interfaces/blog-data';
import { TestimonialData } from './interfaces/testimonial-data';
import { FunData } from './interfaces/fun-data';
import { CoursesData } from './interfaces/courses-data';

@Injectable({
    providedIn: 'root'
})
export class SiteDataService {
    data = data;

    constructor() { }

    getAboutData(): AboutData {
        return this.data.about;
    }

    getContactData(): CardData {
        return this.data.contact;
    }

    getProjectData(): ProjectData[] {
        return this.data.projects;
    }

    getProjectOverviews(): CardData[] {
        let data = this.getProjectData();
        let retVal = []
        for (let i = 0; i < data.length; i++) {
            let newData = {
                "title": data[i].title,
                "image": data[i].overview.image,
                "contents": data[i].overview.contents
            }
            retVal.push(newData);
        }
        return retVal;
    }

    getProjectDetailFromTitle(title: string): ProjectInfoData | null {
        let f = (check: ProjectData) => {
            return check.title === title;
        }

        return this.getProjectDetail(f);
    }

    getProjectDetailFromPath(path: string): ProjectInfoData | null {
        let f = (check: ProjectData) => {
            return check.details.path === path;
        }

        return this.getProjectDetail(f);
    }

    getProjectDetail(checkFunc: Function): ProjectInfoData | null {
        let data = this.getProjectData();

        for (let i = 0; i < data.length; i++) {
            let check = data[i];
            if (checkFunc(check)) {
                let retVal = {
                    "title": check.title,
                    "backgroundFile": check.details.backgroundFile,
                    "technologyFile": check.details.technologyFile,
                    "links": check.details.links,
                    "images": check.details.images,
                    "path": check.details.path,
                }

                return retVal;
            }
        }

        // Title not found
        return null;
    }

    getBlogData(): BlogData[] {
        return this.data.blogs;
    }

    getPublicBlogData(): BlogData[] {
        let blogs = this.getBlogData();
        let retVals = [];
        
        for (let i = 0; i < blogs.length; i++) {
            if (blogs[i].public) {
                retVals.push(blogs[i]);
            }
        }

        return retVals;
    }

    getBlogPost(year: number, month: number, day: number, path: string): BlogData | null {
        let posts = this.getBlogData();

        for (let i = 0; i < posts.length; i++) {
            let check = posts[i];
            if (check.date.year === year && check.date.month === month && 
                check.date.day === day && check.titlePath === path) {
                return check;
            }
        }

        // Blog post not found
        return null;
    }

    getTestimonials(): TestimonialData[] {
        return this.data.testimonials;
    }

    getFunData(): FunData {
        return this.data.fun;
    }

    getCoursesData(school: string | null): CoursesData | null {
        switch (school) {
            case "undergrad": {
                return this.data.courses.undergrad;
            }
            case "high_school": { 
                return this.data.courses.high_school;
            }
            default: {
                return null;
            }
        }
    }
}