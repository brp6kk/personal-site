# [Personal Site](https://www.ericyc.dev)

Portfolio site for me (Eric Y Chang)

## Built with
* [Angular](https://angular.io/)
* [Sass](https://sass-lang.com/)
* [Bootstrap](https://getbootstrap.com/)